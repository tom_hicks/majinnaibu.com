var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  var db = req.db;
  console.dir(db);
  //var latestPost = db.blog.find().sort({published:1}).limit(1);
  // console.dir(latestPost); 
  res.render('index', { title: 'Express' });
});

module.exports = router;
